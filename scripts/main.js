"use strict"; //strict mode is a way to introduce better error-checking into your code 

// Питання. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

// Віддповідь. setTimeout() виконує дію через вказаний проміжок часу, а setInterval() повторює виконання дії кожен вказаний проміжок часу (регулярно)

// Питання.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

// Віддповідь. Така дія планує виконатись якнайшвидше, але виконання буде зразу після завершення виконання поточного скрипту 

// Питання.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

// Віддповідь. для його зупинки

let allImages = document.querySelectorAll(".image-to-show");
let btnstop = document.querySelector(".btn-stop");
let btncontinue = document.querySelector(".btn-continue");

let firstImage = 0;
let timeOut = 0;

const showImage = () => {
    allImages[firstImage].style.display = "none";
    firstImage = (firstImage + 1) % allImages.length;
    allImages[firstImage].style.display = "block";
    timeOut = setTimeout(showImage, 3000);
};

timeOut = setTimeout(showImage, 1000);

btnstop.addEventListener("click", () => {
    clearTimeout(timeOut);
});

btncontinue.addEventListener("click", () => {
    timeOut = setTimeout(showImage, 3000);
});
